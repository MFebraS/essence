<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// social login
Route::prefix('social-login')->group(function () {
	Route::post('/{provider}', 'SocialLoginController@socialLogin');
	Route::get('/{provider}/callback', 'SocialLoginController@callback');
});

Route::get('/{any}', function () {
    return view('layouts.vue');
})->where('any', '.*');
