<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*======= user =======*/
Route::prefix('user')->group(function(){
	Route::post('register', 'Api\ApiUserController@register');
	Route::post('password/forget', 'Api\ApiUserController@forgetPassword');
	Route::post('password/reset', 'Api\ApiUserController@resetPassword');

	Route::middleware('auth:api')->group(function(){
	    Route::get('detail', 'Api\ApiUserController@detail');
	    Route::post('update', 'Api\ApiUserController@update');
	    Route::get('logout', 'Api\ApiUserController@logout');
	});
});

/*======= topic =======*/
Route::prefix('topic')->group(function(){
	Route::get('/', 'Api\ApiTopicController@index');

	Route::middleware('auth:api')->group(function(){
		Route::post('add', 'Api\ApiTopicController@store');
	});
});

/*======= post =======*/
Route::prefix('post')->group(function(){
	Route::get('/', 'Api\ApiPostController@index');
	Route::get('popular', 'Api\ApiPostController@popularPost');
	Route::get('author/{name}', 'Api\ApiPostController@authorPost');
	Route::get('topic/{name}', 'Api\ApiPostController@topic');
	Route::get('detail/{slug}', 'Api\ApiPostController@detail');
	Route::post('search/{type?}', 'Api\ApiPostController@search');

	Route::middleware('auth:api')->group(function(){
		Route::get('user', 'Api\ApiPostController@userPost');
		Route::get('user/detail/{slug}', 'Api\ApiPostController@userDetail');
		
		Route::post('add', 'Api\ApiPostController@store');
		Route::post('edit', 'Api\ApiPostController@edit');
		Route::post('update', 'Api\ApiPostController@update');
		Route::post('delete', 'Api\ApiPostController@destroy');

		Route::post('upload-image', 'Api\ApiPostController@uploadImage');
	});
});