<?php

use Illuminate\Database\Seeder;

class TopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('topics')->truncate();

    	$topics = [
    		"Art",
    		"Automotive",
    		"Beauty",
    		"Business",
    		"Culture",
    		"Design",
    		"Economic",
    		"Fiction",
    		"Food",
    		"Geographic",
    		"Health",
    		"Law",
    		"Novel",
    		"Movie",
    		"Travel",
    		"Science",
    		"Sport",
    	];

    	$data = [];
    	foreach ($topics as $key => $value) {
    		$item = [
    			'name' => $value,
                'created_at' => now(),
                'updated_at' => now(),
    		];

    		array_push($data, $item);
    	}

        DB::table('topics')->insert($data);
    }
}
