<?php

use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('posts')->truncate();
        DB::table('post_topics')->truncate();

        $faker = \Faker\Factory::create();

        for($i=0; $i<=30; $i++):
        	$title = rtrim($faker->sentence(4), '.');

        	$slug = strtolower($title);
            $slug = str_replace(' ', '-', $slug);

            DB::table('posts')
                ->insert([
                    'user_id' 	 => ($i % 5) + 1,
                    'title'		 => $title,
                    'slug' 		 => $slug,
                    'image' 	 => null,
                    'content' 	 => $faker->paragraph(20),
                    'status' 	 => 'Publish',
                    'view' 		 => $i,
                    'post_date'  => now(),
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);

        	// seeds post_topics table
	        for($k=0; $k<=2; $k++):
	            DB::table('post_topics')
	                ->insert([
	                    'post_id' 	 => $i + 1,
	                    'topic_id' 	 => $faker->numberBetween(1, 17),
	                    'created_at' => now(),
	                    'updated_at' => now(),
	                ]);
	        endfor;
        endfor;

    }
}
