<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $this->call(PassportSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(TopicSeeder::class);
        $this->call(PostSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
