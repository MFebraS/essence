<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->truncate();

    	DB::table('users')
            ->insert([
                'name' 		 => "User A",
                'email' 	 => "user.a@mail.com",
                'image' 	 => null,
                'password' 	 => bcrypt('123456'),
                'created_at' => now(),
                'updated_at' => now(),
            ]);

        $faker = \Faker\Factory::create();

        for($i=0; $i<=5; $i++):
            DB::table('users')
                ->insert([
                    'name' 		 => $faker->name,
                    'email' 	 => $faker->email,
                    'image' 	 => null,
                    'password' 	 => bcrypt('123456'),
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
        endfor;
    }
}
