<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PassportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('oauth_clients')->truncate();
    	DB::table('oauth_personal_access_clients')->truncate();
    	
        DB::table('oauth_clients')->insert([
        	[
        		'id' => 1,
	        	'user_id' => null,
	        	'name' => 'Essence Personal Access Client',
	        	'secret' => 'RCiZyd3OZv5GjTeUO21DmJ7qRJjLAbwSamLcqHdy',
	        	'redirect' => 'http://localhost',
	        	'personal_access_client' => 1,
	        	'password_client' => 0,
	        	'revoked' => 0,
	        	'created_at' => date('Y-m-d H:i:s'),
	        	'updated_at' => date('Y-m-d H:i:s')
	        ],
	        [
        		'id' => 2,
	        	'user_id' => null,
	        	'name' => 'Essence Password Grant Client',
	        	'secret' => 'ns6XHOW8GyF7n7XaOZ64SPqV7yM0VRzitwMk4ve9',
	        	'redirect' => 'http://localhost',
	        	'personal_access_client' => 0,
	        	'password_client' => 1,
	        	'revoked' => 0,
	        	'created_at' => date('Y-m-d H:i:s'),
	        	'updated_at' => date('Y-m-d H:i:s')
	        ]
        ]);

        DB::table('oauth_personal_access_clients')->insert([
        	'id' => 1,
        	'client_id' => 1,
        	'created_at' => date('Y-m-d H:i:s'),
        	'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
