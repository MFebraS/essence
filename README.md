## About
Essence is a simple blog build with Laravel v 7.0, Vue Js v 2.5.x, and MySQL.

## Installation
- composer install
- sudo chmod 777 -R storage
- create .env file
- php artisan key:generate
- create database
- php artisan migrate
- php artisan db:seed
- php artisan passport:install
- npm install
- npm run watch

## Features
- CSS SASS:
	- variable
	- nested selector
	- @mixin and @include
	- nowrap & ellipsis (CSS vanilla)
- Vue Js:
	- vue router
		- params, props, query
		- protect auth route
			- `router.beforeEach`
	- components
		- same component different route
			- `<router-view :key="$route.path"></router-view>` replaced:
				watch `$route(to, from)`
		- emit (pass data to parent component)
	- props, data, computed, created, mounted, watch, methods, directives
	- v-show, v-for, v-if, v-else
	- listen bootstrap event in vue component
	- axios
		- interceptor
		- refresh token
	- encrypt-decrypt
	- storage (cookies, local, & session storage)
	- vuex
		- modules
	- preview & upload image
	- infinite scroll
	- scroll to top
	- Social Login
	- Lazy load image
- Laravel:
	- Custom Auth
	- Validation
		- required_with
		- required_without
	- Use storage for upload
	- Accessor in model
		- get original value `getAttributes()['image']`
	- Scope in model
		- relationship with scope
	- Faker in seeder
	- Cron job
		- Custom command
	- Mail
	- Social Login
- Multi select + free input