## Custom Auth Laravel Passport
1. Install Laravel Passport
2. Update User model
3. Update config/auth.php 	(if User model location change)
	- guards > api > driver
	- providers > model
4. Update AuthServiceProvider.php
	- You can add scope here
5. Create AccessTokenController.php to customize issueToken
7. Add CreateFreshApiToken in Kernel.php > web middleware (optional)