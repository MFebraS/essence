<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\CronJob\PostCron;

class RemovePostImageTemp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:remove-post-image-temp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove post image temp after 2 days from last update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $command = PostCron::removePostImageTemp();

        return $this->info($command);
    }
}
