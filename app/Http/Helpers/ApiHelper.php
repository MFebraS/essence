<?php

namespace App\Http\Helpers;

use Storage;

class ApiHelper {
	static function checkCreate($data, $error_messages=null)
	{
		if ($data) {
			return response()->json([
				"status" => "success",
				"result" => $data
			]);
		}
		else {
			if ($error_messages == null)
				$error_messages = ["Add data failed"];

			return response()->json([
				"status" => "fail",
				"messages" => $error_messages
			]);
		}
	}

	static function checkGet($data, $error_messages=null)
	{
		if ($data) {
			return response()->json([
				"status" => "success",
				"result" => $data
			]);
		}
		else {
			if ($error_messages == null)
				$error_messages = ["Data not found"];

			return response()->json([
				"status" => "fail",
				"messages" => $error_messages
			]);
		}
	}

	static function checkUpdate($data, $error_messages=null)
	{
		if ($data) {
			return response()->json([
				"status" => "success",
				"result" => $data
			]);
		}
		else {
			if ($error_messages == null)
				$error_messages = ["Update data failed"];

			return response()->json([
				"status" => "fail",
				"messages" => $error_messages
			]);
		}
	}

	static function checkDelete($data, $error_messages=null)
	{
		if ($data == true || $data > 0) {
			return response()->json([
				"status" => "success",
				"result" => $data
			]);
		}
		else {
			if ($error_messages == null)
				$error_messages = ["Delete data failed"];

			return response()->json([
				"status" => "fail",
				"messages" => $error_messages
			]);
		}
	}

	/*
	 * upload base 64 or blob file
	 */
	static function uploadFile($file, $path, $filename)
	{
		if (is_string($file)) {
	    	// get file extension
	    	$file_extension = explode('/', mime_content_type($file))[1];
	    	
			// decode base 64
	    	$base64_index = strpos($file, 'base64,');
	    	$file = base64_decode(substr($file, $base64_index + 7));
		}
		else {
			$file_extension = $file->getClientOriginalExtension();
			$file = file_get_contents($file);
		}
    	
	    $filename = $filename .'.'. $file_extension;

    	// upload base 64 file
    	Storage::put($path .'/'. $filename, $file);

    	return $path ."/". $filename;
	}

}