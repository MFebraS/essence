<?php

namespace App\Http\Controllers\CronJob;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\PostImageTemp;
use Carbon\Carbon;
use Log;
use Storage;

class PostCron extends Controller
{
	/*
	 * remove post image temp older than 2 days ago
	 */
	public static function removePostImageTemp()
	{
		try {
	    	$post_image_temps = PostImageTemp::whereDate('updated_at', '<=', Carbon::now()->subDays(2)->toDateTimeString())->take(100);

	    	$temp = $post_image_temps->get();
	    	$image_deleted = [];
	    	foreach ($temp as $key => $item) {
	    		if (Storage::exists($item->image)) {
	                Storage::delete($item->image);

	                array_push($image_deleted, $item->image);
	            }
	    	}

	    	$delete = $post_image_temps->delete();

	    	$date = date('Y-m-d H:i:s');
	    	Log::channel('post')->info("===============================");
	    	Log::channel('post')->info("Date : " . $date);
	    	Log::channel('post')->info("Deleted Row : ". $delete);
	    	Log::channel('post')->info("Deleted Image : " . count($image_deleted));
	    	Log::channel('post')->info("\t" . json_encode($image_deleted) . "\n");

	    	return "true";

		} catch (Exception $e) {
	    	$date = date('Y-m-d H:i:s');
			Log::channel('post')->error("===============================");
	    	Log::channel('post')->error("Date : " . $date);
			Log::channel('post')->error("Error Message : " . $e->getMessage() . "\n");

			return "false";
		}
	}
}
