<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\ApiHelper;
use App\Models\User;
use Carbon\Carbon;
use Socialite;

class SocialLoginController extends Controller
{
    public function socialLogin($provider)
    {
    	try {
            $social_user = Socialite::driver($provider)->stateless()->user();

            $user = User::firstOrCreate([
                'email' => $social_user->getEmail()
            ], [
                'socialite_id' => $social_user->getId(),
                'socialite_name' => $provider,
                'name' => $social_user->getName(),
                'image' => $social_user->getAvatar(),
                'email_verified_at' => now()
            ]);

            // create passport token
            $token_result = $user->createToken('Personal Access Token');
            // store the created token
            $token = $token_result->token;
            // add a expiration date to the token
            $token->expires_at = Carbon::now()->addDays(1);
            // save the token to the user
            $token->save();

            $data = [
                'access_token' => $token_result->accessToken,
                'user' => $user
            ];

            return ApiHelper::checkGet($data, ["User not found"]);
        }
        catch (\Exception $e) {
            return response()->json([
                "status"   => "fail",
                "messages" => [$e->getMessage()]
            ]);
        }
    }

    public function callback()
    {
    	return true;
    }
}
