<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use \Laravel\Passport\Http\Controllers\AccessTokenController as PassportAccessToken;
use App\Models\User;
use Validator;
use Exception;

class AccessTokenController extends PassportAccessToken
{
    public function issueToken(ServerRequestInterface $request)
    {
        $grantType = $request->getParsedBody()['grant_type'];
        
        $rules = [
            "grant_type" => "required",
            "scope" => "required",
            "client_id" => "required",
            "client_secret" => "required",
        ];
        if ($grantType == "password") {
            $rules["username"] = "required|email";
            $rules["password"] = "required|min:6";
        }

    	$validator = Validator::make($request->getParsedBody(), $rules);

        if ($validator->fails()) {
        	return response()->json([
            	"status" => "fail",
            	"messages" => $validator->errors()->all()
            ]);
        }

        try {

            // if request bearer login
            if ($grantType == "password") {
                // get username (default is email)
                $username = $request->getParsedBody()['username'];

                // get user
                $user = User::where('email', '=', $username)->firstOrFail()->toArray();
            }

            // issuetoken
            $tokenResponse = parent::issueToken($request);

            // convert response to json string
            $content = $tokenResponse->getContent();

            // convert json to array
            $data = json_decode($content, true);

            if(isset($data["error"]))
                throw new OAuthServerException('The user credentials were incorrect.', 6, 'invalid_credentials', 401);

            if ($grantType == "password")
                // add user in response
                $data['user'] = $user;

            return response()->json([
            	"status" => "success",
            	"result" => $data
            ]);
        }
        catch (ModelNotFoundException $e) { // email not found
            // return error message
            return response()->json([
            	"status" => "fail",
            	"messages" => ["Email or password is incorrect"]
            ]);
        }
        catch (OAuthServerException $e) { // password not correct..token not granted
            // return error message
            return response()->json([
            	"status" => "fail",
            	"messages" => ["Email or password is incorrect"]
            ]);
        }
        catch (Exception $e) {
            // return error message
            return response()->json([
            	"status" => "fail",
            	"messages" => [$e->getMessage()],
                "code" => "AT01"
            ]);
        }
    }
}
