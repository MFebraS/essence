<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Helpers\ApiHelper;
use App\Models\Post;
use App\Models\PostTopic;
use App\Models\PostImage;
use App\Models\PostImageTemp;
use App\Models\Topic;
use App\Models\User;
use Auth;
use DB;
use Storage;
use Validator;

class ApiPostController extends Controller
{
    public function index()
    {
    	$posts = Post::with(['user', 'postTopics.topic'])
            ->exclude(['content', 'view'])  // exclude fields to minimize json size
    		->where('status', 'Publish')
            ->orderBy('post_date', 'desc')
            ->paginate(10);

    	return ApiHelper::checkGet($posts, ['Retrieve post failed']);
    }

    public function search(Request $request, $type=null)
    {
        $req = $request->all();

        $validator = Validator::make($req, [
            "keyword" => "required",
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status"   => "fail",
                "messages" => $validator->errors()->all()
            ]);
        }

        $keyword = '%' . $req['keyword'] . '%';

        // define exclude fields
        $exclude = ['content', 'view'];
        if ($type == "quick") {
            $exclude = ['content', 'view', 'post_date', 'image'];
        }

        $posts = Post::exclude($exclude)
            ->where('status', 'Publish')
            ->where(function($query) use($keyword) {
                $query->where('title', 'like', $keyword)
                    ->orWhere('content', 'like', $keyword);
            })
            ->orderBy('post_date', 'desc');

        if ($type == "quick") {
            $posts = $posts->take(10)->get();
        }
        else {
            $posts = $posts->with(['user', 'postTopics.topic'])->paginate(10);
        }

        return ApiHelper::checkGet($posts, ['Retrieve post failed']);
    }

    public function popularPost()
    {
        $posts = Post::with(['user', 'postTopics.topic'])
            ->exclude(['content', 'updated_at'])  // exclude fields to minimize json size
            ->where('status', 'Publish')
            ->orderBy('view', 'desc')
            ->take(5)
            ->get();

        return ApiHelper::checkGet($posts, ['Retrieve popular post failed']);
    }
    
    // get posts by author/ user
    public function authorPost($name)
    {
        $user = User::where('name', $name)->first();
        if (empty($user)) {
            return response()->json([
                "status"   => "fail",
                "messages" => ['Author not found']
            ]);
        }

        $posts = Post::with(['user', 'postTopics.topic'])
            ->exclude(['content', 'view'])  // exclude fields to minimize json size
            ->where([
                'user_id' => $user->id,
                'status'  => 'Publish'
            ])
            ->orderBy('post_date', 'desc')
            ->paginate(10);

        return ApiHelper::checkGet($posts, ['Retrieve post failed']);
    }

    // get post by topic
    public function topic($name)
    {
        $topic = Topic::where('name', $name)->first();
        if (empty($topic)) {
            return response()->json([
                "status"   => "fail",
                "messages" => ['Topic not found']
            ]);
        }

        $post_topics = PostTopic::with('post.user')
            ->where('topic_id', $topic->id)
            ->latest()
            ->paginate(10)
            ->toArray();

        // refactor result
        // take out posts and make as data
        $posts = [];
        foreach ($post_topics['data'] as $key => $item) {
            array_push($posts, $item['post']);
        }

        $post_topics['data'] = $posts;

        return ApiHelper::checkGet($post_topics, ['Retrieve post failed']);
    }

    // get user's post
    public function userPost()
    {
        $posts = Post::with(['user', 'postTopics.topic'])
            ->where('user_id', Auth::id())
            ->orderBy('post_date', 'desc')
            ->paginate(10);

        return ApiHelper::checkGet($posts, ['Retrieve post failed']);
    }

    public function detail($slug)
    {
        $post = Post::with(['user', 'postTopics.topic'])
            ->where([
                'slug'   => $slug,
                'status' => 'Publish'
            ])
            ->first();

        if (!empty($post)) {
            // update view count
            $post->increment('view');
        }

        return ApiHelper::checkGet($post, ['Post not found']);
    }

    // get user's post detail
    public function userDetail($slug)
    {
        $post = Post::with(['user', 'postTopics.topic'])
            ->where([
                'slug'    => $slug,
                'user_id' => Auth::id()
            ])
            ->first();

        return ApiHelper::checkGet($post, ['Post not found']);
    }

    // upload temp image from ckeditor
    public function uploadImage(Request $request)
    {
        $req = $request->all();

        $validator = Validator::make($req, [
            "image" => "required",
        ]);

        if ($validator->fails()) {
            return response()->json([
                "error"   => true,
                "message" => $validator->errors()->all()
            ]);
        }

        $upload_path = 'public/post-image-temp';
        $file_name = 'temp - '. time();
        $file_path = ApiHelper::uploadFile($req['image'], $upload_path, $file_name);

        $post_image_temp = new PostImageTemp;
        $post_image_temp->post_id = $req['post_id'] ?? null;
        $post_image_temp->image   = $file_path;
        $post_image_temp->save();

        return response()->json([
            "url"   => asset('storage/app/' . $file_path)
        ]);
    }

    public function store(Request $request)
    {
        $req = $request->all();

        $validator = Validator::make($req, [
            "title"       => "required",
            "status"      => "required|in:Draft,Publish",
            "image"       => "required",
            "content"     => "required",
            "topic_ids"   => "required_without:topic_names|array",
            "topic_names" => "required_without:topic_ids|array",
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status"   => "fail",
                "messages" => $validator->errors()->all()
            ]);
        }

        $topic_ids = $req['topic_ids'] ?? [];

        DB::beginTransaction();
            // add new topics
            if (isset($req['topic_names'])) {
                $new_topic_ids = [];
                foreach ($req['topic_names'] as $key => $value) {
                    $new_topic = Topic::firstOrCreate(['name' => $value]);
                    $new_topic_ids[] = $new_topic->id;
                }

                $topic_ids = array_unique(array_merge($topic_ids, $new_topic_ids));
            }

            // create slug
            $slug = strtolower($req['title']);
            $slug = str_replace(' ', '-', $slug);

            // check unique slug
            $check_slug = Post::where('slug', $slug)->first();
            while (!empty($check_slug)) {
                $new_slug = $slug . '-' . strtolower(Str::random(8));
                $check_slug = Post::where('slug', $new_slug)->first();
            }

            // add post
            $post = new Post;
            $post->user_id   = Auth::id();
            $post->title     = $req['title'];
            $post->slug      = isset($new_slug) ? $new_slug : $slug;
            $post->status    = $req['status'];
            $post->content   = $req['content'];
            $post->post_date = date('Y-m-d H:i:s');
            $post->save();

            // upload image
            $upload_path = 'public/posts';
            $file_name = $post->id .' - '. time();
            $file_path = ApiHelper::uploadFile($req['image'], $upload_path, $file_name);

            // content image
            $content = $this->processContentImage($post->id, $req['content']);

            // update post image & content
            $post->image   = $file_path;
            $post->content = $content;
            $post->save();

            // add post topic
            if (isset($topic_ids)) {
                foreach ($topic_ids as $key => $topic_id) {
                    $post_topic = new PostTopic;
                    $post_topic->post_id  = $post->id;
                    $post_topic->topic_id = $topic_id;
                    $post_topic->save();
                }
            }

        DB::commit();

        return ApiHelper::checkCreate($post, ["Add post failed"]);
    }

    public function edit(Request $request)
    {
        $req = $request->all();

        $validator = Validator::make($req, [
            "post_id" => "required",
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status"   => "fail",
                "messages" => $validator->errors()->all()
            ]);
        }

        $post = Post::with(['user', 'postTopics.topic'])
            ->where([
                'id'      => $req['post_id'],
                'user_id' => Auth::id()
            ])
            ->first();

        if (empty($post)) {
            return response()->json([
                "status"   => "fail",
                "messages" => ['Post not found']
            ]);
        }

        $data['post']   = $post;
        $data['topics'] = Topic::orderBy('name')->get();

        return ApiHelper::checkGet($data, ['Retrieve post failed']);
    }

    public function update(Request $request)
    {
        $req = $request->all();

        $validator = Validator::make($req, [
            "post_id"     => "required",
            "title"       => "required",
            "status"      => "required|in:Draft,Publish",
            "content"     => "required",
            "topic_ids"   => "required_without:topic_names|array",
            "topic_names" => "required_without:topic_ids|array",
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status"   => "fail",
                "messages" => $validator->errors()->all()
            ]);
        }
        
        $post = Post::where([
                    'id'      => $req['post_id'],
                    'user_id' => Auth::id()
                ])
                ->first();

        if (empty($post)) {
            return response()->json([
                "status"   => "fail",
                "messages" => ['Post not found']
            ]);
        }

        $topic_ids = $req['topic_ids'] ?? [];

        DB::beginTransaction();
            // remove old PostTopic
            PostTopic::where('post_id', $req['post_id'])->delete();

            // add new topics
            if (isset($req['topic_names'])) {
                $new_topic_ids = [];
                foreach ($req['topic_names'] as $key => $value) {
                    $new_topic = Topic::firstOrCreate(['name' => $value]);
                    $new_topic_ids[] = $new_topic->id;
                }

                $topic_ids = array_unique(array_merge($topic_ids, $new_topic_ids));
            }

            // if post title not change, use old slug
            if ($post->title === $req['title']) {
                $slug = $post->slug;
            }
            else {
                $slug = strtolower($req['title']);
                $slug = str_replace(' ', '-', $slug);
            }

            // check unique slug
            $check_slug = Post::where([
                                ['id', '!=', $post->id],
                                ['slug', '=', $slug]
                            ])
                            ->first();

            while (!empty($check_slug)) {
                $new_slug = $slug . '-' . strtolower(Str::random(8));
                $check_slug = Post::where('slug', $new_slug)->first();
            }

            // upload image
            if (isset($req['image'])) {
                $upload_path = 'public/posts';
                $file_name = $post->id .' - '. time();
                $file_path = ApiHelper::uploadFile($req['image'], $upload_path, $file_name);

                // remove old image
                if (Storage::exists( $post->getAttributes()['image'] )) {
                    Storage::delete( $post->getAttributes()['image'] );
                }

                // update post image
                $post->image = $file_path;
            }

            // content image
            $content = $this->processContentImage($post->id, $req['content']);

            // update post
            $post->title     = $req['title'];
            $post->slug      = isset($new_slug) ? $new_slug : $slug;
            $post->status    = $req['status'];
            $post->content   = $content;
            $post->post_date = date('Y-m-d H:i:s');
            $post->save();

            // add post topic
            if (isset($topic_ids)) {
                foreach ($topic_ids as $key => $topic_id) {
                    $post_topic = new PostTopic;
                    $post_topic->post_id  = $post->id;
                    $post_topic->topic_id = $topic_id;
                    $post_topic->save();
                }
            }

        DB::commit();

        return ApiHelper::checkCreate($post, ["Update post failed"]);
    }

    private function processContentImage($post_id, $content)
    {
        // get all image url
        $pattern = '@src="([^"]+)"@';
        preg_match_all($pattern, $content, $matches);

        $temp_images = [];
        $new_content = $content;

        foreach ($matches[1] as $key => $value) {
            // check if image name in temp directory or not
            $position = strpos($value, '/temp');
            if ($position !== false) {
                $file_name     = substr($value, $position + 1);
                $new_file_name = str_replace('temp', $post_id, $file_name);

                $file_dir     = 'post-image-temp/' . $file_name;
                $new_file_dir = 'post-images/' . $new_file_name;

                // move image name to post_images table
                $post_image = new PostImage;
                $post_image->post_id = $post_id;
                $post_image->image   = 'public/' . $new_file_dir;
                $post_image->save();

                // replace image temp in content
                $new_content = str_replace($file_dir, $new_file_dir, $new_content);

                $from = storage_path('app/public/') . $file_dir;
                $to   = storage_path('app/public/') . $new_file_dir;

                if (Storage::exists('public/' . $file_dir)) {
                    // move image from temp directory
                    rename($from, $to);

                    array_push($temp_images, 'public/' . $file_dir);
                }
            }
        }

        // remove temp images from table
        PostImageTemp::whereIn('image', $temp_images)->delete();

        // remove previous image (if deleted in ckeditor)
        $post_images = PostImage::where('post_id', $post_id)->get();
        $old_ids = [];
        foreach ($post_images as $key => $item) {
            $check_img = strpos($new_content, $item->image);
            // if image already not in content
            if ($check_img === false) {
                array_push($old_ids, $item->id);

                if (Storage::exists($item->image)) {
                    Storage::delete($item->image);
                }
            }
        }
        if (!empty($old_ids)) {
            PostImage::whereIn('id', $old_ids)->delete();
        }

        return $new_content;
    }

    public function destroy(Request $request)
    {
        $req = $request->all();

        $validator = Validator::make($req, [
            "post_id" => "required",
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status"   => "fail",
                "messages" => $validator->errors()->all()
            ]);
        }

        $post = Post::where([
                    'id'      => $req['post_id'],
                    'user_id' => Auth::id()
                ])
                ->first();
        if (empty($post)) {
            return response()->json([
                "status"   => "fail",
                "messages" => ['Post not found']
            ]);
        }

        // remove image
        if (Storage::exists( $post->getAttributes()['image'] )) {
            Storage::delete( $post->getAttributes()['image'] );
        }

        $delete = $post->delete();

        return ApiHelper::checkGet($delete, ['Delete post failed']);
    }

}
