<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Helpers\ApiHelper;
use App\Models\Topic;
use Validator;

class ApiTopicController extends Controller
{
    public function index()
    {
        $topics = Topic::orderBy('name')->get();

        return ApiHelper::checkGet($topics, ["Retrieve topic failed"]);
    }

    public function store(Request $request)
    {
    	$post = $request->all();

        $validator = Validator::make($post, [
            "name" => "required|unique:topics",
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status" => "fail",
                "messages" => $validator->errors()->all()
            ]);
        }

        $topic = new Topic;
        $topic->name = $post['name'];
        $topic->save();

        return ApiHelper::checkCreate($topic, ["Add topic failed"]);
    }
}
