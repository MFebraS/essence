<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Helpers\ApiHelper;
use App\Mail\ForgetPassword;
use App\Models\User;
use Auth;
use DB;
use Hash;
use Log;
use Mail;
use Storage;
use Str;
use Validator;

class ApiUserController extends Controller
{
    public function register(Request $request)
    {
        $post = $request->all();

        $validator = Validator::make($post, [
            "name"     => "required",
            "email"    => "required|email|unique:users",
            "password" => "required|confirmed|min:6"
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status"   => "fail",
                "messages" => $validator->errors()->all()
            ]);
        }

        $user = new User;
        $user->name     = $post['name'];
        $user->email    = $post['email'];
        $user->password = bcrypt($post['password']);
        $user->save();

        return ApiHelper::checkCreate($user, ["Register failed"]);
    }

    /*
     * create token & send it to email
     */
    public function forgetPassword(Request $request)
    {
        $post = $request->all();

        $validator = Validator::make($post, [
            "email"    => "required|email",
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status"   => "fail",
                "messages" => $validator->errors()->all()
            ]);
        }

        $user = User::where('email', $post['email'])->first();
        if (empty($user)) {
            return response()->json([
                "status"   => "fail",
                "messages" => ["Email not found"]
            ]);
        }

        $password_reset_token = Hash::make(Str::random(32));
        $check_token = User::where('password_reset_token', $password_reset_token)->first();
        while (!empty($check_token)) {
            $password_reset_token = Hash::make(Str::random(32));
            $check_token = User::where('password_reset_token', $password_reset_token)->first();
        }

        $token_expired = date("Y-m-d H:i:s", strtotime('+24 hours'));

        $user->password_reset_token   = $password_reset_token;
        $user->password_token_expired = $token_expired;
        $user->save();

        $link = env('APP_URL') . '/auth/reset-password?key=' . $password_reset_token;

        Mail::to($user->email)->send(new ForgetPassword($user, $link));

        if (count(Mail::failures()) > 0) {
            return response()->json([
                "status"   => "fail",
                "messages" => ["Failed to send email", "Please check your email address"]
            ]);
        }

        return response()->json([
            "status"   => "success",
            "messages" => ["Link for reset password has been successfully send to your email"]
        ]);
    }

    public function resetPassword(Request $request)
    {
        $post = $request->all();

        $validator = Validator::make($post, [
            "token"    => "required",
            "password" => "required|confirmed|min:6",
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status"   => "fail",
                "messages" => $validator->errors()->all()
            ]);
        }

        $user = User::where('password_reset_token', $post['token'])
            ->whereDate('password_token_expired', '>=', date('Y-m-d H:i:s'))
            ->first();
        if (empty($user)) {
            return response()->json([
                "status"   => "fail",
                "messages" => ["Token not found or expired"]
            ]);
        }

        $user->password_reset_token = null;
        $user->password_token_expired = null;
        $user->password = bcrypt($post['password']);
        $user->save();

        return response()->json([
            "status"   => "success",
            "messages" => ["Reset password success"]
        ]);
    }

    public function detail()
    {
        $user = Auth::user();

        return ApiHelper::checkGet($user, ["User not found"]);
    }

    public function update(Request $request)
    {
        $post = $request->all();

        $validator = Validator::make($post, [
            "name"         => "required|string",
            "old_password" => "required_with:password|min:6",
            "password"     => "required_with:old_password|min:6|confirmed",
        ]);

        if ($validator->fails()) {
            return response()->json([
                "status"   => "fail",
                "messages" => $validator->errors()->all()
            ]);
        }

        $user = Auth::user();
        
        if (isset($post['old_password'])) {
        	// check old password
        	if (Hash::check($post['old_password'], $user->password)) {
		        $user->password = bcrypt($post['password']);
        	}
        	else {
        		return response()->json([
	                "status"   => "fail",
	                "messages" => ['Old password is invalid']
	            ]);
        	}
        }

        // upload image
        if (isset($post['image'])) {
            $upload_path = 'public/users';
            $file_name = $user->id .' - '. time();
            $file_path = ApiHelper::uploadFile($post['image'], $upload_path, $file_name);

            // remove old image
            if (Storage::exists( $user->getAttributes()['image'] )) {
                Storage::delete( $user->getAttributes()['image'] );
            }

            $user->image = $file_path;
        }

        $user->name = $post['name'];
        $user->save();

        return ApiHelper::checkUpdate($user, ["Update user failed"]);
    }

    public function logout()
    {
        $user = Auth::user();
        if ($user) {
            $delete = DB::table('oauth_access_tokens')->where('user_id', $user->id)->delete();
            if ($delete > 0) {
                return response()->json([
                    'status'   => 'success',
                    'result'   => $delete,
                    'messages' => ['Logout success']
                ]);
            }

            return response()->json([
                'status'   => 'fail',
                'messages' => ['Logout failed']
            ]);
        }

        return response()->json([
            'status'   => 'fail',
            'messages' => ['User not found']
        ]);
    }
}
