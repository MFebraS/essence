<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'user_id',
        'title',
        'slug',
        'image',
        'content',
        'status',
        'view',
        'post_date',
    ];

    protected $columns = [
        'id',
        'user_id',
        'title',
        'slug',
        'image',
        'status',
        'view',
        'post_date',
    ];

    /*
     * accessor image field
     * get full url of image
    */
    public function getImageAttribute($value)
    {
        if (empty($value)) {
            return $value;
        }
        return asset('storage/app/' . $value);
    }

    // exclude some attributes
    public function scopeExclude($query, $value=[] ) 
    {
        return $query->select( array_diff($this->columns, (array) $value) );
    }

    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }

    public function postTopics()
    {
    	return $this->hasMany('App\Models\PostTopic');
    }

    public function postImages()
    {
        return $this->hasMany('App\Models\PostImage');
    }
}
