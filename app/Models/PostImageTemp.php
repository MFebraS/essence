<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostImageTemp extends Model
{
    protected $fillable = [
    	'post_id',
		'image'
    ];
}
