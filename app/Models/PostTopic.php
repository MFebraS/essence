<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostTopic extends Model
{
    protected $fillable = [
        'post_id',
        'topic_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    // relationship with scope
    public function post()
    {
    	return $this->belongsTo('App\Models\Post')->exclude(['content']);
    }

    public function topic()
    {
    	return $this->belongsTo('App\Models\Topic');
    }
}
