<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ env('APP_NAME') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            .btn-wrapper {
                padding: 15px;
            }
            .btn {
                width: 135px;
                padding: 10px 20px;
                background-color: #1fa672;
                color: #fff;
                border: none;
                border-radius: 4px;
                display: block;
                margin-left: auto;
                margin-right: auto;
                box-sizing: border-box;
                text-align: center;
                font-size: 14px;
                font-weight: bold;
            }
            a.btn {
                text-decoration: none;
            }
            .footer {
                margin-top: 30px;
                padding: 20px;
                border-top: 1px solid #ccc;
                background-color: #f5f5f5;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div>
            <p>Hi {{ $user->name }},</p>
            <p>You are receiving this email because of reset password request.</p>
            <p>Please click this button below to reset your password.</p>
            <div class="btn-wrapper">
                <a class="btn" href="{{ $link }}">Reset Password</a>
            </div>
            <p>If you <b>did not</b> request reset password, please <b>ignore</b> this email.</p>

            <br>

            <div>Kind Regards</div>
            <div><b>{{ env('APP_NAME') }}</b></div>

            <br>

            <div class="footer">
                &copy; {{ env('APP_NAME') . " " . date('Y') }}
            </div>
        </div>
    </body>
</html>
