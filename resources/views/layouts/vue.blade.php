<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie-edge">
        <!-- csrf token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>{{ env('APP_NAME') }}</title>

		<link rel="stylesheet" type="text/css" href="{{ asset('public/css/app.css') }}">
	</head>

	<body>
		<div id="app"></div>

		<script type="text/javascript" src="{{ asset('public/js/app.js') }}"></script>
	</body>
</html>