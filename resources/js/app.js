import './bootstrap'
import 'es6-promise/auto' // required by vuex

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueSocialauth from 'vue-social-auth'
import router from './router'
import store from './store'

import App from './layouts/App.vue'

Vue.use(Vuex)
Vue.use(VueAxios, axios)
Vue.use(VueSocialauth, {
	providers: {
		google: {
			clientId: process.env.MIX_GOOGLE_CLIENT_ID,
			redirectUri: process.env.MIX_GOOGLE_REDIRECT_URL
		},
		facebook: {
			clientId: process.env.MIX_FACEBOOK_APP_ID,
			redirectUri: process.env.MIX_FACEBOOK_REDIRECT_URL
		}
	}
})

const vueApp = new Vue({
	router,
	store,
	el: '#app',
	render: h => h(App)
});

