import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'
import general from './modules/general'
import post from './modules/post'
import topic from './modules/topic'
import user from './modules/user'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
	modules: {
		general,
		post,
		topic,
		user,
	},
	strict: debug,
	plugins: debug ? [ createLogger() ] : []
})