import { getData, setData, clearData } from '../../helpers/Storage'

// initial state
const state = () => ({
	user: getData('data') || null
})

// getters
const getters = {}

// mutations
const mutations = {
	setUser(state, data) {
		state.user = data
		setData('data', data)
	},
	clearUser(state) {
		state.user = null
		clearData('data')
        clearData('act')
        clearData('rft')
	}
}

// actions
const actions = {
	setUser({ commit }, data) {
		commit('setUser', data)
	},
	clearUser({ commit }) {
		commit('clearUser')
	}
}

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}