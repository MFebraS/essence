// general state
const state = () => ({
	search_image	: process.env.MIX_BASE_ROUTE + '/public/images/search.svg',
	user_image		: process.env.MIX_BASE_ROUTE + '/public/images/user.svg',
	post_image		: process.env.MIX_BASE_ROUTE + '/public/images/post.jpg',
	tag_image		: process.env.MIX_BASE_ROUTE + '/public/images/tag.svg',
	load_image		: process.env.MIX_BASE_ROUTE + '/public/images/loader.svg',
	load_image_2	: process.env.MIX_BASE_ROUTE + '/public/images/loader-2.gif',
	arrow_up_image	: process.env.MIX_BASE_ROUTE + '/public/images/arrow-up.svg',
	not_found_image	: process.env.MIX_BASE_ROUTE + '/public/images/404.jpg',
	loading			: 0
})

// mutations
const mutations = {
	showLoading(state) {
		state.loading += 1
	},
	hideLoading(state) {
		state.loading -= 1
	}
}

export default {
	namespaced: true,
	state,
	mutations,
}