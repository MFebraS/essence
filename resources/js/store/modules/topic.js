import { getData, setData } from '../../helpers/Storage'

// initial state
const state = () => ({
	topics: getData('topics', false, "session") || []
})

// getters
const getters = {}

// mutations
const mutations = {
	setTopics (state, data) {
		state.topics = data
		setData('topics', data, false, "session")
	}
}

// actions
const actions = {
	setTopics({ commit }, data) {
		commit('setTopics', data)
	}
}

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}