// initial state
const state = () => ({
	id: null 	// use for ckeditor upload adapter
})

// getters
const getters = {}

// mutations
const mutations = {
	setPostId (state, data) {
		state.id = data
	},
	clearPostId (state) {
		state.id = null
	}
}

// actions
const actions = {
	setPostId({ commit }, data) {
		commit('setPostId', data)
	},
	clearPostId({ commit }) {
		commit('clearPostId')
	}
}

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions
}