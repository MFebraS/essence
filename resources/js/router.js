import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store'

import Home from './views/Home.vue'
import Post from './views/Post.vue'
import Search from './views/Search.vue'
import Profile from './views/user/Profile.vue'
import ProfileEdit from './views/user/ProfileEdit.vue'
import MyPost from './views/user/MyPost.vue'
import PostAdd from './views/user/PostAdd.vue'
import PostEdit from './views/user/PostEdit.vue'
import PageNotFound from './views/PageNotFound.vue'
import ForgetPassword from './views/auth/ForgetPassword.vue'
import ResetPassword from './views/auth/ResetPassword.vue'

Vue.use(VueRouter);

const router = new VueRouter({
	mode: 'history',
	base: process.env.MIX_BASE_ROUTE,
	routes: [
		{
			path: '/',
			name: 'home',
			component: Home
		},
		{
			path: '/author/:author',
			name: 'author',
			component: Home
		},
		{
			path: '/topic/:topic',
			name: 'topic',
			component: Home
		},
		{
			path: '/post/:slug',
			name: 'post',
			component: Post
		},
		{
			path: '/search',
			name: 'search',
			component: Search
		},
		{
			path: '/auth/forget-password',
			name: 'forget-password',
			component: ForgetPassword
		},
		{
			path: '/auth/reset-password',
			name: 'reset-password',
			component: ResetPassword
		},
		{
			path: '/user/profile',
			name: 'profile',
			component: Profile,
			props: true,
			meta: {
				auth: true
			}
		},
		{
			path: '/user/profile/edit',
			name: 'edit-profile',
			component: ProfileEdit,
			meta: {
				auth: true
			}
		},
		{
			path: '/user/post',
			name: 'user-post',
			component: MyPost,
			props: true,
			meta: {
				auth: true
			}
		},
		{
			path: '/user/post/new',
			name: 'user-new-post',
			component: PostAdd,
			meta: {
				auth: true
			}
		},
		{
			path: '/user/post/edit/:id',
			name: 'user-edit-post',
			component: PostEdit,
			meta: {
				auth: true
			}
		},
		{
			path: '/user/post/detail/:slug',
			name: 'user-post-detail',
			component: Post,
			meta: {
				auth: true
			}
		},
		{
			path: '*',
			name: 'not-found',
			component: PageNotFound
		},
	],
	scrollBehavior (to, from, savedPosition) {
		return { x: 0, y: 0 }
	}
});

router.beforeEach((to, from, next) => {
	// guard route
	const user = store.state.user.user
	if (to.meta.auth && !user) next({ name: 'home' })
	else next()
})

export default router;