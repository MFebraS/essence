import router from '../router'
import store from '../store'
import { setData, getData } from './Storage'

const app_url = process.env.MIX_APP_URL	  // base url
const api_url = process.env.MIX_API_URL	  // base url + api
const timeout = 5000 	// 5 seconds timeout

// set authorization header
let access_token = getData('act')
if (access_token) {
	axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token
}

/*
 * GET method for request API
 */
export const get = async (url, api=true) => {
	url = (api ? api_url : app_url) + url
	console.log('======= GET url :', url)

	try {
		const response = await axios({
			method	: 'get',
			url 	: url,
			timeout : timeout,
			headers : {
				'Accept': 'application/json',
			}
		})

		// request success
		console.log('======= GET response :', response)
		return response

	} catch (e) {
		// request fail
		console.error('======= error :', e.message)
		return {
			status 	 : 'fail',
			messages : [e.message]
		}
	}
}

/*
 * POST method for request API
 */
export const post = async (url, params, api=true, type="json") => {
	url = (api ? api_url : app_url) + url
	console.log('======= POST url :', url)
	console.log('======= POST params :', params)

	let headers = {
		'Accept': 'application/json'
	}

	if (type === "form-data") {
		headers['Content-Type'] = 'multipart/form-data'

		// create form data
		let form_data = new FormData()
		Object.entries(params).forEach(entry => {
			let [key, value] = entry

			if (value) {
				if (Array.isArray(value)) {
					value.forEach( function(element, index) {
						form_data.append(`${key}[]`, element);
					})
				}
				else {
					form_data.append(key, value)
				}

				console.log('======= POST params form :', key, value)
			}
		})

		params = form_data
	}

	try {
		const response = await axios({
			method	: 'post',
			url 	: url,
			timeout : timeout,
			headers : headers,
			data 	: params
		})

		// request success
		console.log('======= POST response :', response)
		return response

	} catch (e) {
		// request fail
		console.error('======= error :', e.message)
		return {
			status 	 : 'fail',
			messages : [e.message]
		}
	}
}

/*
 * response interceptors
 * extract and return response.data
 * refresh token if unauthenticated or redirect to guest route
 */
const resp_interceptors = axios.interceptors.response.use(response => {
	return response.data
},
async (error) => {
	console.log('--- error ', error)
	const original_request = error.config

	if (error.response && error.response.status === 401) {
		// unauthenticated on first try
		if (!original_request.retry) {
			original_request.retry = true

			access_token = await refreshAccessToken()
			axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token
		
			return axios(original_request)
		}
		else {
			delete axios.defaults.headers.common['Authorization']

			// clear data
			store.dispatch('user/clearUser')

			// redirect route
            router.push({ name: 'home' }).catch(() => {})
		}
	}

	return Promise.reject(error)
})

const refreshAccessToken = async () => {
	let url = app_url + '/oauth/token'

	let params = {
		grant_type	  : 'refresh_token',
		scope 		  : '*',
		client_id	  : process.env.MIX_PASSWORD_ID,
		client_secret : process.env.MIX_PASSWORD_SECRET,
		refresh_token : getData('rft')
	}

	try {
		const response = await axios({
			method	: 'post',
			url 	: url,
			timeout : timeout,
			headers : { 'Accept': 'application/json' },
			data 	: params
		})

		// request success
		console.log('======= refreshAccessToken response :', response)
		// update token in storage
		setData('act', response.result.access_token)
		setData('rft', response.result.refresh_token)

		return response.result.access_token

	} catch (e) {
		// request fail
		console.error('======= error :', e.message)
		return {
			status 	 : 'fail',
			messages : [e.message]
		}
	}
}

