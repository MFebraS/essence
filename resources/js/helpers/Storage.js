import Cookies from 'js-cookie'

const domain = process.env.MIX_DOMAIN
const cryptr_key = process.env.MIX_CRYPT_KEY	// should 16 chars

const encryptor = require('simple-encryptor')(cryptr_key)

export const setData = (key, value, encrypt=true, type="cookies") => {
	if (value) {
		if (encrypt) {
			// encrypt also include stringify
			value = encryptor.encrypt(value)
		}
		else {
			// stringify
			value = JSON.stringify(value)
		}
	}

	// save data
	switch (type) {
		case "local":
			localStorage.setItem(key, value)
			break
		case "session":
			sessionStorage.setItem(key, value)
			break
		default:
			Cookies.set(key, value, {
				expires: 7,
				path: '/',
				domain: domain,
				sameSite: 'Strict'
			})
			break
	}
}

export const getData = (key, decrypt=true, type="cookies") => {
	let value = null
	// get data
	switch (type) {
		case "local":
			value = localStorage.getItem(key)
			break
		case "session":
			value = sessionStorage.getItem(key)
			break
		default:
			value = Cookies.get(key)
			break
	}

	if (value) {
		if (decrypt) {
			// decrypt also include json parse
			value = encryptor.decrypt(value)
		}
		else {
			// parse string
			value = JSON.parse(value)
		}
	}

	return value;
}

export const clearData = (key, type="cookies") => {
	switch (type) {
		case "local":
			localStorage.removeItem(key)
			break
		case "session":
			sessionStorage.removeItem(key)
			break
		default:
			Cookies.remove(key, {
				path: '/',
				domain: domain,
				sameSite: 'Strict'
			})
			break
	}
}