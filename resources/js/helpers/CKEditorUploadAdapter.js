import store from '../store'
import { getData } from './Storage'

export default class CKEditorUploadAdapter {
	constructor( loader, post_id ) {
        this.loader  = loader;
    }

    // start upload file
    upload() {
        return this.loader.file
            .then( file => new Promise( ( resolve, reject ) => {
                this._initRequest();
                this._initListeners( resolve, reject, file );
                this._sendRequest( file );
            } ) );
    }

    // abort upload file
    abort() {
        if ( this.xhr ) {
            this.xhr.abort();
        }
    }

    _initRequest() {
        const xhr = this.xhr = new XMLHttpRequest();

        // Note that your request may look different. It is up to you and your editor
        // integration to choose the right communication channel. This example uses
        // a POST request with JSON as a data structure but your configuration
        // could be different.
        const url = process.env.MIX_API_URL + '/post/upload-image'
        xhr.open( 'POST', url, true );
        xhr.responseType = 'json';
    }

    _initListeners( resolve, reject, file ) {
        const xhr = this.xhr;
        const loader = this.loader;
        const genericErrorText = `Couldn't upload file: ${ file.name }.`;

        xhr.addEventListener( 'error', () => reject( genericErrorText ) );
        xhr.addEventListener( 'abort', () => reject() );
        xhr.addEventListener( 'load', () => {
            const response = xhr.response;

            // This example assumes the XHR server's "response" object will come with
            // an "error" which has its own "message" that can be passed to reject()
            // in the upload promise.
            //
            // Your integration may handle upload errors in a different way so make sure
            // it is done properly. The reject() function must be called when the upload fails.
            if ( !response || response.error ) {
                return reject( response && response.error ? response.error.message : genericErrorText );
            }

            // If the upload is successful, resolve the upload promise with an object containing
            // at least the "default" URL, pointing to the image on the server.
            // This URL will be used to display the image in the content. Learn more in the
            // UploadAdapter#upload documentation.
            resolve( {
                default: response.url
            } );
        } );

        // Upload progress when it is supported. The file loader has the #uploadTotal and #uploaded
        // properties which are used e.g. to display the upload progress bar in the editor
        // user interface.
        if ( xhr.upload ) {
            xhr.upload.addEventListener( 'progress', evt => {
                if ( evt.lengthComputable ) {
                    loader.uploadTotal = evt.total;
                    loader.uploaded = evt.loaded;
                }
            } );
        }
    }

    _sendRequest( file ) {
		let csrf_token = null
		// get csrf token from meta
		const metas = document.getElementsByTagName('meta')
		for (let i = 0; i < metas.length; i++) {
			if (metas[i].getAttribute('name') === 'csrf-token') {
				csrf_token = metas[i].getAttribute('content')
				break
			}
		}
		const access_token = 'Bearer ' + getData('act')

    	this.xhr.setRequestHeader('X-CSRF-TOKEN', csrf_token);
    	this.xhr.setRequestHeader('Authorization', access_token);
    	this.xhr.setRequestHeader('Accept', 'application/json');

        // Prepare the form data.
        const data = new FormData();

		const post_id = store.state.post.id
        if (post_id) {
        	data.append('post_id', post_id);
        }
        data.append('image', file);

        // Important note: This is the right place to implement security mechanisms
        // like authentication and CSRF protection. For instance, you can use
        // XMLHttpRequest.setRequestHeader() to set the request headers containing
        // the CSRF token generated earlier by your application.

        // Send the request.
        this.xhr.send( data );
    }

    /*xhr.addEventListener( 'load', () => {
	    const response = xhr.response;

	    // ...

	    // response.urls = {
	    // 	default: 'http://example.com/images/image–default-size.png',
	    // 	'160': '...',
	    // 	'500': '...',
	    // 	// ...
	    // 	'1052': 'http://example.com/images/image–default-size.png'
	    // }
	    resolve( response.urls );
	} );*/

}