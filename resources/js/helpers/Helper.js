export const limitChars = (string, limit) => {
	if (string.length > limit) {
		string = string.slice(0, limit) + '...';
	}

	return string;
}

export const kFormatter = (num) => {
    return Math.abs(num) > 999 ? Math.sign(num)*((Math.abs(num)/1000).toFixed(1)) + 'k' : Math.sign(num)*Math.abs(num)
}

// e.g. : a = [1,2,3]  b = [1,2,3]
export const arrayEquals = (a, b) => {
	return a.length === b.length &&
		a.every((val, index) => val === b[index]);
}